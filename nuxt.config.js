export default {
  css: [
    'normalize.css/normalize.css',
    '@/assets/styles/_base.scss'
  ],
  modules: [
    'nuxt-webfontloader',
    '@nuxtjs/style-resources',
    '@nuxtjs/svg',
    '@nuxtjs/pwa',
    '@nuxtjs/google-analytics'
  ],
  plugins: [
    '@/plugins/vue-lazyload'
  ],
  styleResources: {
    scss: [
      '@/assets/styles/_variables.scss',
      '@/assets/styles/mixins/_mq.scss',
      '@/assets/styles/mixins/_visually-hidden.scss'
    ]
  },
  pwa: {
    manifest: {
      short_name: 'bd',
      name: 'bd | Front-end Developer',
      display: 'standalone'
    },
    meta: {
      name: 'bd | Front-end Developer',
      author: '@bartdominiak',
      description: 'I\'m Bartek, and I\'m webdeveloper based in Poznan, Poland. I\'ve been creating websites professionally for over four years, and still loves every minute of it. Currently working at SNOW.DOG as a Front-end Developer.',
      ogHost: 'https://bartdominiak.pl',
      ogImage: '/images/og-logo.png'
    }
  },
  'google-analytics': {
    id: 'UA-151382420-1'
  },
  webfontloader: {
    google: {
      families: ['Lato:300:latin&display=swap']
    }
  },
  build: {
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
